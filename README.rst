vagrant-php-dev-box
===================

Introduction
------------

A setup for Vagrant to help you with getting started with a PHP development box.

Getting Started
---------------

In order to install the required Cookbooks there is a dependency on librarian.
You can install the required gem easily::

    [sudo] gem install librarian

Once the gem is installed all you will need to run is `librarian-chef`. Once executed
this will download and install the appropriate cookbooks for the project.

There is also a dependency on two vagrant plugins both of which can be installed
with the following commands::

    vagrant plugin install vagrant-omnibus
    vagrant plugin install vagrant-hostmanager

Author
------

Marucs Ramsden <marcus@beyondthecorner.co.uk>
